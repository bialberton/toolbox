#!/bin/bash

if [ -z "$1" ]; then
cat <<EOU
Finds the first Euler number given by Freesurfer when it is correcting the defects.
Usage: 
	fsEuler <folder path> <subjectList.txt> <outputName.txt> : RECOMENDED - Process only the subjects on the list and save in <outpuName.txt>
	fsEuler <folder path> : Process all subfolders as subjects
	fsEuler <subjectList.txt> : Process all subfolders in \$SUBJECTS_DIR
	fsEuler <folder path> <subjectList.txt> : Process only the subjects on the list and save Euler numbers in <folder path>/fsEulerRes.txt

Output: <subj_folderPath> <lh euler number> <rh euler number>
_____________________________________
Bianca A. V. Alberton
Federal University of Technology - Paraná / Imaging and Electronic Instrumentation Laboratory (LABIEM)
v1.01 - Jan/2019
EOU
exit 1
fi

#Finds the folder with the subject given by the user
if [ -d "$1" ]; then
	echo "Found the folder with the subjects in " $1
	fsDir=$1
	if [ -z "$2" ]; then
		find $fsDir -maxdepth 1 -mindepth 1 -type d | awk -F "/" '{print $NF}' > $fsDir/fsEulerSL.txt
		subjList="$fsDir/fsEulerSL.txt"
		echo "Creating a subjects list in $fsDir/fsEulerSL.txt"
	else
		subjList=$2
		echo "Found subjects list in " $subjList
	fi
else
	fsDir="$SUBJECTS_DIR"
	subjList=$1
	echo "Searching for subjects in" $fsDir
fi


#When using a subjects list, it always saves the results in a .txt file (or another extension if specified by the user, such as .csv)
if [ -z "$3" ]; then
	resultsFile="$fsDir/fsEulerRes.txt"
	if [ -f "$resultsFile" ]; then
		rm $resultsFile
		echo "removed old $resultsFile"
	fi
else 
	resultsFile=$3
fi

echo "Saving Euler numbers into" $resultsFile

#For each subject in the list search the euler number in the recon-all.log
for F in $(cat $subjList)
do 
	if [ -d "$fsDir/$F" ]; then
		raFile=$(find $fsDir/$F -type f -name "recon-all-lh.log")
		#echo $raFile
		# If there is no recon-all-lh.log, the data should be stored into a a single recon-all.log
		if [ -z "$raFile" ]; then
			#echo $F "Searching Euler numbers in recon-all.log"

			raFile=$(find $fsDir/$F -type f -name "recon-all.log")
			#echo $raFile
			if [ -z "$raFile" ];then
				echo $F "File recon-all.log not found"
			fi

			#Extract info about Euler number
			resAwk=$(awk '/After retessellation of defect 0/ {eulerN=$8; sub(/#=/,"",eulerN); print eulerN}' $raFile)
			echo ${raFile%/scripts/recon-all.log} $resAwk  >> $resultsFile
			
		else
			#echo $F 'Searching Euler numbers in recon-all-lh.log and recon-all-lh.log'
			
			resLH=$(awk '/After retessellation of defect 0/ {eulerN=$8; sub(/#=/,"",eulerN); print eulerN}' $raFile)

			#Checks if the info from right hemisphere is also available
			raFile=$(find $fsDir/$F -type f -name "recon-all-rh.log")
			if [ -z "$raFile" ]; then 
				echo $F "File recon-all-rh.log not found"
			else
				resRH=$(awk '/After retessellation of defect 0/ {eulerN=$8; sub(/#=/,"",eulerN); print eulerN}' $raFile)
				echo ${raFile%/scripts/recon-all-rh.log} $resLH $resRH  >> $resultsFile
			fi
		fi
	else
		echo $F "not found"
	fi 
done

