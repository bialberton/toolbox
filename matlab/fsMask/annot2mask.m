function annot2mask(annotFileName,maskFileName)
% Reads annotation files and create a mask with only cortex values
% Dependencies: FreeSurfer
%               Anderson's toolbox (download: https://github.com/andersonwinkler/toolbox)

%%%% Copied from Anderson's function annot2dpv.m
% Some FS commands are needed now
fspath = getenv('FREESURFER_HOME');
if isempty(fspath),
    error('FREESURFER_HOME variable not correctly set');
else
    addpath(fullfile(fspath,'matlab'));
end

% Read the annotation file
[~,lab,ctab] = read_annotation(annotFileName);

% For each structure, replace its coded colour by its index
for s = 1:ctab.numEntries,
    lab(lab == ctab.table(s,5)) = s;
end

%Gets only values that are not labelled as Unknown - label 1 or 0 (label 0 is a FreeSurfer bug) - or
%corpus-callosum -label 5
mask = ~(lab == 0 | lab == 1 | lab == 5);

% Save the result
dpxwrite(maskFileName,mask)

