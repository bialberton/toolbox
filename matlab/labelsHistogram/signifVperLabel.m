% filePath='palm_results/lh_cmi_fwhm20_dpx_tstat_fwep_m1_c24.mgz';
% imgOutFolder = 'summaryPerLabel';
% alpha=0.05;
% log10Alpha=true;
function [totalSignif,nVperLabel,percentValues] = signifVperLabel(filePath, imgOutFolder, alpha, log10Alpha)
% Calculates the number of significant vertices per label considering the
% fsaverage5 from FreeSurfer and saves its histogram.
% Uses palm_miscread(filePath) to read the image. Please refer to PALM 
% documentation to see image types compatible at 
% https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/PALM/UserGuide
%
% Usage:
% [totalSignif,nVperLabel,percentValues] = signifVperLabel(filePath, imgOutFolder, alpha, log10Alpha)
% 
% Inputs: 
% filePath     : Image path
% imgOutFolder : Folder path where the histograms will be stored
% alpha        : significance level
% log10Alpha   : true if the image values are stored in in -log10(alpha),
%                false otherwise.

%
% Outputs:
% totalSignif  : total number of significant vertices in the image
% nVperLabel   : number of significant vertices per label (ROI)
% percentValues: proportion of significant vertices in relation to the size
%                of the ROI
%

labelsTxt = {'unknown'; 'bankssts'; 'caudalanteriorcingulate'; 'caudalmiddlefrontal';...
    'corpuscallosum'; 'cuneus'; 'entorhinal'; 'fusiform'; 'inferiorparietal';...
    'inferiortemporal'; 'isthmuscingulate'; 'lateraloccipital'; ...
    'lateralorbitofrontal'; 'lingual'; 'medialorbitofrontal'; 'middletemporal'; ...
    'parahippocampal'; 'paracentral'; 'parsopercularis'; 'parsorbitalis'; ...
    'parstriangularis'; 'pericalcarine'; 'postcentral'; 'posteriorcingulate'; ...
    'precentral'; 'precuneus'; 'rostralanteriorcingulate'; 'rostralmiddlefrontal'; ...
    'superiorfrontal'; 'superiorparietal'; 'superiortemporal'; 'supramarginal'; ...
    'frontalpole'; 'temporalpole'; 'transversetemporal'; 'insula'}';
nLabels = size(labelsTxt,2);

[~,fileName,~] = fileparts(filePath);
if strcmp(fileName(1:2),'rh')
    load('labelRH.mat','labels');
else
    load('labelLH.mat','labels');
end

%Get Brain image
img = getfield(palm_miscread(filePath),'data');

%Evaluate the number of significant vertices
if log10Alpha
    signifVertices = img >= -log10(alpha);
    %fprintf(1,'Number of significant vertices in this image: %d \n', sum(signifVertices));
else
    signifVertices = img <= alpha;
end

totalSignif= sum(signifVertices);
nVperLabel = zeros(1,nLabels);
percentValues = nVperLabel;

if totalSignif > 0
    %checks if the folder exists
    if(exist(imgOutFolder,'dir') ~= 7)
        mkdir(imgOutFolder);
    end
    
    signifLabels = labels(signifVertices);
    C = categorical(signifLabels,(1:nLabels),labelsTxt);

    % Generates the histogram of significant vertices/voxels per label in
    % absolute values
    figure(1); set(gcf, 'Position', get(0, 'Screensize'));
    h = histogram(C);
    nVperLabel = h.Values;
    title(sprintf('Number of significant vertices per ROI in image %s, %d significant vertices in total',fileName, sum(signifVertices)));
    ylabel('Number of significant vertices');
    xtickangle(90);
  
    % Includes the proportion of significant vertices/voxels per label
    [percentLabel, ~,~] = histcounts(labels);
    percentValues = nVperLabel./percentLabel*100;
    
    strValues = cell(1,nLabels);
    for i = 1: nLabels
        if nVperLabel(i) > 0
            strValues{i} = sprintf("%d \n(%2.2f%%)",nVperLabel(i),percentValues(i));
        else
            strValues{i} = sprintf("%d",nVperLabel(i));
        end
    end

    text((1:nLabels), nVperLabel+ 0.03*max(nVperLabel), strValues, 'HorizontalAlignment', 'center');
    saveas(gcf, fullfile(imgOutFolder,sprintf('%s_signifLabels.svg',fileName)));
    close(1);
    
end

% edges = (1:37);
% 
% [nVperLabel, ~,~] = histcounts(signifLabels,edges);
% [percentLabel, ~,~] = histcounts(labels,edges);
% percentValues = nVperLabel./percentLabel*100;
% 
% %Generates the histogram if there are significant values only
% if sum(nVperLabel) > 0 
%     % Generates the histogram of significant vertices/voxels per label in
%     % absolute values
%     figure(1); set(gcf, 'Position', get(0, 'Screensize'));
%     h = bar(labelsTxt, nVperLabel);
%     title(sprintf('Number of significant vertices per ROI in image %s, %d significant vertices in total',fileName, sum(signifVertices)));
%     ylabel('Number of significant vertices');
%     text(labelsTxt, nVperLabel+ 0.03*max(nVperLabel), string(nVperLabel), 'HorizontalAlignment', 'center');
%     xtickangle(90);
% 
%     %Save image
%     %checks if the folder exists
%     if(exist(imgOutFolder,'dir') ~= 7)
%         mkdir(imgOutFolder);
%     end
% 
%     saveas(gcf, fullfile(imgOutFolder,sprintf('%s_signifLabels.svg',fileName)));
%     close(1);
% 
%     % Generates the histogram of the proportion of significant vertices/voxels 
%     % per label
%     figure(1); set(gcf, 'Position', get(0, 'Screensize'));
% 
%     h = bar(labelsTxt, percentValues);
%     title(sprintf('Proportion of significant vertices per ROI in image %s',fileName));
%     ylabel('Proportion of significant vertices inside the ROI (%)');
%     strValues = string(percentValues);
%     strValues = cellfun(@(x) strcat(x, "%"),strValues);
%     text(labelsTxt, percentValues+ 0.03*max(percentValues), strValues, 'HorizontalAlignment', 'center');
%     xtickangle(90);
% 
%     saveas(gcf, fullfile(imgOutFolder,sprintf('%s_signifLabelsPercent.svg',fileName)));
%     close(1);
% end